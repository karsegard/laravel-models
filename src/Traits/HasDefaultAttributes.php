<?php

namespace KDA\Laravel\Models\Traits;

use DB;

/*
implementation of 
http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
*/

trait HasDefaultAttributes
{

    static public function bootHasDefaultAttributes(): void
    {
    
        static::creating(function ($model)
        {
           $model->setDefaultValues();
        });
        static::updating(function ($model)
        {
           $model->setDefaultValues();
        });
    }
    
}


 
<?php

namespace KDA\Laravel\Models\Traits;


use Carbon\Carbon;
use DB;
trait Versionnable
{

    use Introspect;

    public function initializeVersionnable(): void
    {
        if (!property_exists($this, 'versionnable')) {
            throw new \Error('versionnable property not defined');
        }

        $this->casts[$this->versionnable['version']] = 'integer';

    }


    public function scopeLastest($query)
    {
        $versionAttributeName = $this->versionnable['version'];
        $table = self::tableName();
        $maxQuery = DB::table($table)
                    ->select("id",DB::raw("max(".$versionAttributeName.") as rev"))
                    ->groupBy('id');

        return $query->innerJoinSub($maxQuery,'b',function($join){
            $join->on($table.'.id','=','b.id');
        });
      
    }

}

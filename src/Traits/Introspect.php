<?php 

namespace KDA\Laravel\Models\Traits;


use Illuminate\Support\Facades\Schema;


trait Introspect
{
    public static function tableName()
    {
        return with(new static)->getTable();
    }

    public static function columns($withPrefix=false,$prefix=null,$separator='.'){
        $table_name =self::tableName();
        if(empty($prefix)){
            $prefix = $table_name;
        }

        return collect(Schema::getColumnListing($table_name))->mapWithKeys(function($item) use($withPrefix,$prefix,$separator){
            $value = $item;
            if($withPrefix){
                $value = $prefix.$separator.$item;                
            }
            return [$item=>$value];
        });
    }
}

<?php

namespace KDA\Laravel\Models\Traits;



trait SvgRepresentation
{

   

    public static function bootSvgRepresentation()
	{
        if (!property_exists(static::class, 'svgs')) {
            throw new \Error('svgs property not defined');
        }
        
	}

    public function initializeSvgRepresentation(): void
    {
        
        if (!property_exists($this, 'svg_attribute')) {
            throw new \Error('svg_attribute property not defined');
        }
    }
    
    static public function getSvgFor($key)
    {
        $svg_attrs = attribute_bag(self::$svg_tag_attributes['svg']);
        $path_attrs = attribute_bag(self::$svg_tag_attributes['path']);
        
        return "<svg  ".$svg_attrs.">
        <style>
            ".self::$svg_stylesheet."

        </style>
                <g>
                <path ".$path_attrs." d=\"" . self::$svgs[$key] . "\"/>
                </g>
            </svg>";
    }

    public function getSvgAttribute()
    {

        return self::getSvgFor($this->{$this->svg_attribute});
    }
}

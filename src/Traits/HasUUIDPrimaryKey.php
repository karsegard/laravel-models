<?php

namespace KDA\Laravel\Models\Traits;


use Illuminate\Database\Eloquent\Model;

use Str;

trait HasUUIDPrimaryKey
{

    public static function boot()

    {
        parent::boot();

        static::creating(function (Model $model) {

            $model->setKeyType('string');

            $model->setIncrementing(false);

            $model->setAttribute($model->getKeyName(), Str::uuid());
        });
    }
}

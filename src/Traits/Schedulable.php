<?php

namespace KDA\Laravel\Models\Traits;


use Carbon\Carbon;

trait Schedulable
{

    public function initializeSchedulable(): void
    {
        if (!property_exists($this, 'schedulable')) {
            throw new \Error('schedulable property not defined');
        }

        $this->casts[$this->schedulable['start']] = 'date';
        $this->casts[$this->schedulable['end']] = 'date';
        $this->casts[$this->schedulable['enabled']] = 'boolean';
    }

    public static function schedulableQuery($key = NULL)
    {

        $m = (new static);

        return self::where($m->schedulable['enabled'], 1)

            ->where(function ($query) use ($m, $key) {
                $m->schedulableGroup($query, $key);
            })
            ->where(function ($query) use ($m, $key) {
                $query->where($m->schedulable['start'], '<=', \Date::now()->format('Y-m-d'))
                    ->orWhereNull($m->schedulable['start']);
            })
            ->where(function ($query) use ($m) {
                $query->where($m->schedulable['end'], '>=', \Date::now()->format('Y-m-d'))
                    ->orWhereNull($m->schedulable['end']);
            })
            ->orderBy($m->schedulable['start'], 'desc')
            ->orderBy($m->schedulable['end'], 'desc');
    }

    public function scopeCurrent($query, $key = NULL)
    {
        return $query->where($this->schedulable['enabled'], 1)


            ->where(function ($query) use ($key) {
                $query->where($this->schedulable['start'], '<=', \Date::now()->format('Y-m-d'))
                    ->orWhereNull($this->schedulable['start']);
            })
            ->where(function ($query) {
                $query->where($this->schedulable['end'], '>=', \Date::now()->format('Y-m-d'))
                    ->orWhereNull($this->schedulable['end']);
            });
    }

    public function scopeForDate($query, $date)
    {
        return $query->where(function ($query) use ($date) {
            $query->where($m->schedulable['start'], '<=', $date)
                ->orWhereNull($m->schedulable['start']);
        })
            ->where(function ($query) use ($date) {
                $query->where($m->schedulable['end'], '>=', $date)
                    ->orWhereNull($m->schedulable['end']);
            });
    }

    public function scopeActive($query)
    {
        return $query->where('enabled', 1);
    }

    public static function schedulableGroup($query, $key)
    {
        if (!isset($m->schedulable['group'])  && $key != NULL) {
            throw new \Error('Provided key with no group defined');
        }
        $groupAttribute = $m->schedulable['group'];
        if (!empty($key)) {
            $query->where($groupAttribute, $key);
        }
    }

    public static function current($key = NULL)
    {
        return self::schedulableQuery($key)->get()->first();
    }

    public function getIsActiveAttribute()
    {
        // dd($this->schedulable,$this->casts);

        $enabledAttribute = $this->schedulable['enabled'];
        $startAttribute = $this->schedulable['start'];
        $endAttribute = $this->schedulable['end'];
        $start = $this->$startAttribute;
        $end = $this->$endAttribute;
        $enabled = $this->$enabledAttribute;
        $group = $this->schedulable_group;
        return $enabled === true && ($start === null || Carbon::now()->greaterThanOrEqualTo($start))  && ($end === null || Carbon::now()->lessThanOrEqualTo($this->$endAttribute)) &&   self::current($group) && self::current($group)->id === $this->id;
    }

    public function getIsPlannedAttribute()
    {
        // dd($this->schedulable,$this->casts);
        $enabledAttribute = $this->schedulable['enabled'];
        $startAttribute = $this->schedulable['start'];
        $endAttribute = $this->schedulable['end'];
        $start = $this->$startAttribute;
        $end = $this->$endAttribute;
        $enabled = $this->$enabledAttribute;
        return (!$this->is_active) && $enabled === true && (Carbon::now()->lessThan($start));
    }


    public function getStartingAttribute()
    {
        $v = $this->attributes[$this->schedulable['start']];
        return  $v !== null ? (new \Date($v))->format('d.m.Y') : '-';
    }

    public function getEndingAttribute()
    {
        $v = $this->attributes[$this->schedulable['end']];
        return  $v !== null ? (new \Date($v))->format('d.m.Y') : 'never';
    }
}

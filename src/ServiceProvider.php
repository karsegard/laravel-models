<?php

namespace KDA\Laravel\Models;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\File;

use KDA\Laravel\PackageServiceProvider;
use KDA\Backpack\DynamicSidebar\Models\Sidebar;
use Illuminate\Support\Facades\View;

class ServiceProvider extends PackageServiceProvider
{

    use \KDA\Laravel\Traits\ProvidesBlueprint;
  
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    public function postRegister(){
        $this->blueprints =[
            'nestable'=> function($parent_id_name='parent_id',$constraint=NULL,$onDelete='restrict'){
                $table = $constraint ?? $this->table;
                $this->foreignId($parent_id_name)->nullable()->constrained($table)->onDelete($onDelete);
                $this->unsignedInteger('lft')->nullable()->default(0);
                $this->unsignedInteger('rgt')->nullable()->default(0);
                $this->unsignedInteger('depth')->nullable()->default(0);
            }
        ];
    }
   
}
